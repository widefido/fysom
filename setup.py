#!/usr/bin/env python

from distutils.core import setup

setup(name="fysom",
      description="pYthOn Finite State Machine",
      url="https://github.com/oxplot/fysom",
      version="1.0",
      author="Mansour",
      author_email="mansour@oxplot.com",
      py_modules=["fysom"]
     )
